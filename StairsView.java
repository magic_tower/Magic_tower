package view.entity;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import model.entity.stairs.Stairs;
import view.View;

public class StairsView extends JPanel implements View{


	private static final long serialVersionUID = 6439921326195501593L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\entity\\stairs\\Stairs.png");
	
	private int type;
	
	public StairsView(int type) {
		setSize(48, 48);
		this.type = type;
	}
	
	@Override
	public void show() {
		repaint();
	}

	@Override
	public void draw(Graphics g) {
		int coord_x = type == Stairs.STAIRS_TYPE_UP ? 32 : 0;
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, coord_x, 0, coord_x + 32, 32, null);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		if(type == Stairs.STAIRS_TYPE_UP || type == Stairs.STAIRS_TYPE_DOWN)
			draw(g);
	}

}
