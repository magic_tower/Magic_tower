package view.window;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class SideView extends JPanel {

	private static final long serialVersionUID = 8036071953709962951L;


	private static final ImageIcon IMG = new ImageIcon("src\\model\\window\\side.png");
	
	public SideView() {
		setLayout(null);
		setBounds(new Rectangle(0, 0, MainView.WIDTH, MainView.HEIGHT));
		setSide();
	}

	private void setSide() {
		JPanel jPanel = new JPanel(new GridLayout(0, 18));
		JPanel jPanel2 = new JPanel(new GridLayout(0, 18));
		JPanel jPanel3 = new JPanel(new GridLayout(0, 1));
		JPanel jPanel4 = new JPanel(new GridLayout(0, 1));
		JPanel jPanel5 = new JPanel(new GridLayout(0, 1));
		for (int i = 0; i < 18; i++) {
			jPanel.add(new Side());
			jPanel2.add(new Side());
		}
		for (int i = 0; i < 11; i++) {
			jPanel3.add(new Side());
			jPanel4.add(new Side());
			jPanel5.add(new Side());
		}
		jPanel.setBounds(0, 0, MainView.WIDTH - 5, 48);
		jPanel2.setBounds(0, MainView.HEIGHT - 48 - 28, MainView.WIDTH - 5, 48);
		jPanel3.setBounds(0, 48, 48, MainView.HEIGHT - 28 - 48 * 2);
		jPanel4.setBounds(MainView.WIDTH - 5 - 48, 48, 48, MainView.HEIGHT - 28 - 48 * 2);
		jPanel5.setBounds(48 * 5, 48, 48, MainView.HEIGHT - 28 - 48 * 2);
		add(jPanel);
		add(jPanel2);
		add(jPanel3);
		add(jPanel4);
		add(jPanel5);
	}
	
	public static void main(String[] args) {
		MainView mainView = new MainView();
		mainView.getContentPane().add(new SideView());
		mainView.setVisible(true);
	}
	
	private class Side extends JPanel{
		private static final long serialVersionUID = -6962416497678757790L;
		public Side() {
			setSize(48, 48);
		}
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(IMG.getImage(), 0, 0, 48, 48, 0, 0, 32, 32, null);
		}
	}
}
