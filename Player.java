package model.role.player;

import java.io.Serializable;
import java.util.ArrayList;

import controller.data.SturaMapDate;

/**
 * TODO 玩家类
 * 
 * @author 柳聪灵
 * @date 2019年1月13日
 * @time 下午3:04:42
 * @site 
 * @version 1.0
 */
public class Player implements Serializable {

	/**
	 * 第一种角色
	 */
	public static final int PLAYER_TYPE_ONE = -1;

	/**
	 * 第二种角色
	 */
	public static final int PLAYER_TYPE_TOW = -2;

	/**
	 * 第三种角色
	 */
	public static final int PLAYER_TYPE_THREE = -3;

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 3705556085323800052L;

	/**
	 * 当前所在楼层
	 */
	private int nowFloor;

	/**
	 * 到过最高的楼层
	 */
	private int maxFloor;

	/**
	 * 血量
	 */
	private int red;

	/**
	 * 攻击力
	 */
	private int attack;

	/**
	 * 防御力
	 */
	private int defense;

	/**
	 * 金币
	 */
	private int money;

	/**
	 * 经验
	 */
	private int exp;

	/**
	 * 道具列表
	 */
	private int prop;

	/**
	 * 钥匙数量数组 0黄 1蓝 2红
	 */
	private int[] keySize;

	/**
	 * 坐标数组 0-x 1-y
	 */
	private int[] coord;
	
	private boolean isVip;
	
	public boolean isVip() {
		return isVip;
	}
	
	public void setVip(boolean isVip) {
		this.isVip = isVip;
	}

	private ArrayList<Integer[][]> mapDataList;

	public ArrayList<Integer[][]> getMapDataList() {
		return mapDataList;
	}

	public void addExp(int exp) {
		this.exp += exp;
	}

	public void addMoney(int money) {
		this.money += money;
	}

	/**
	 * 玩家类型 用于读取图片模型
	 */
	private final int type;

	public Player(int type) {
		this.type = type;
		mapDataList = SturaMapDate.srcDataToArray();
		maxFloor = nowFloor = 1;
		red = 1000;
		attack = defense = 10;
		keySize = new int[3];
		coord = new int[2];
		coord[0] = 5;
		coord[1] = 10;
		isVip = false;
	}
	
	/**
	 * 获得角色类型
	 */
	public int getType() {
		return type;
	}

	/**
	 * 获得当前所在楼层
	 */
	public int getNowFloor() {
		return nowFloor;
	}

	/**
	 * 设置当前所在楼层
	 */
	public void setNowFloor(int nowFloor) {
		this.nowFloor = nowFloor;
	}

	/**
	 * 获得到过的最大楼层
	 */
	public int getMaxFloor() {
		return maxFloor;
	}

	/**
	 * 设置到过的最大的楼层
	 */
	public void setMaxFloor(int maxFloor) {
		this.maxFloor = maxFloor;
	}

	/**
	 * 获得血量
	 */
	public int getRed() {
		return red;
	}

	/**
	 * 设置血量
	 */
	public void setRed(int red) {
		this.red = red;
	}

	/**
	 * 获得攻击力
	 */
	public int getAttack() {
		return attack;
	}

	public void setAttack(int attack) {
		this.attack = attack;
	}
	
	/**
	 * 增加攻击力
	 */
	public void addAttack(int attack) {
		this.attack += attack;
	}

	/**
	 * 获得防御力
	 */
	public int getDefense() {
		return defense;
	}

	/**
	 * 增加防御力
	 */
	public void addDefense(int defense) {
		this.defense += defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}
	
	/**
	 * 获得金币数
	 */
	public int getMoney() {
		return money;
	}

	/**
	 * 设置金币数
	 */
	public void setMoney(int money) {
		this.money = money;
	}

	/**
	 * 获得经验值
	 */
	public int getExp() {
		return exp;
	}

	/**
	 * 设置经验值
	 */
	public void setExp(int exp) {
		this.exp = exp;
	}

	/**
	 * 获得道具列表
	 */
	public int getProp() {
		return prop;
	}

	/**
	 * 增加道具
	 */
	public void addProp(int prop) {
		this.prop |= prop;
	}

	/**
	 * 获得指定钥匙类型的数量
	 */
	public int getKeySize(int keyType) {
		keyType += 51;
		keyType = Math.abs(keyType);
		return keySize[keyType];
	}

	/**
	 * 添加指定钥匙
	 */
	public void addKey(int keyType) {
		keyType += 51;
		keyType = Math.abs(keyType);
		this.keySize[keyType]++;
	}

	/**
	 * 使用指定钥匙
	 */
	public boolean useKey(int DoorType) {
		DoorType += 11;
		DoorType = Math.abs(DoorType);
		if (keySize[DoorType] == 0) {
			return false;
		}
		keySize[DoorType]--;
		return true;
	}

	/**
	 * 获得x坐标
	 */
	public int getX() {
		return coord[0];
	}

	/**
	 * 获得y坐标
	 */
	public int getY() {
		return coord[1];
	}

	/**
	 * 设置坐标
	 */
	public void setCoord(int x, int y) {
		coord[0] = x;
		coord[1] = y;
	}

}
