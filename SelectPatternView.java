package view.window;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


public class SelectPatternView extends JPanel {

	private static final long serialVersionUID = 841789688132636543L;

	private static final ImageIcon IMG = new ImageIcon("src/model/window/background.jpg");

	public SelectPatternView() {
		setSize(MainView.WIDTH, MainView.HEIGHT);
		
		setLayout(new FlowLayout(FlowLayout.CENTER, MainView.WIDTH, 30));
		addLabel();
		addButton();
	}

	private void addButton() {
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, MainView.WIDTH / 2, 15));
		panel.setOpaque(false);
		panel.setPreferredSize(new Dimension(MainView.WIDTH, MainView.HEIGHT));
		Font font = new Font("方正舒体", Font.BOLD, 40);
		ImageIcon icon = new ImageIcon("src/model/window/selectBox.png");

		String[] info = {"新的游戏","继续游戏","退出游戏"};
		for (int i = 0; i < info.length; i++) {
			JButton button= new JButton(info[i],icon);
			button.setHorizontalTextPosition(SwingConstants.CENTER);  
			button.setContentAreaFilled(false);//设置图片填满按钮所在的区域  
//			button.setBorderPainted(false);//设置是否绘制边框  
			button.setBorder(null);//设置边框 
			button.setFont(font);
			button.addMouseListener(new ExcColorListeren(button));
			panel.add(button);
		}

		add(panel);
	}

	public void addButtonsListener(ActionListener[] listeners) {
		Component[] coms = ((JPanel) getComponents()[1]).getComponents();
		for (int i = 0; i < coms.length; i++) {
			((JButton)coms[i]).addActionListener(listeners[i]);
		}
	}

	private void addLabel() {
		JLabel label = new JLabel("魔塔");
		label.setFont(new Font("华文行楷", Font.ITALIC, 150));
		add(label);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(IMG.getImage(), 0, 0, getWidth(), getHeight(), 0, 0, IMG.getIconWidth(), IMG.getIconHeight(), null);
	}
	
	private class ExcColorListeren implements MouseListener {

		JButton button;

		public ExcColorListeren(JButton button) {
			this.button = button;
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
			button.setForeground(Color.darkGray);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			button.setForeground(Color.blue);
		}

	}
	
}
