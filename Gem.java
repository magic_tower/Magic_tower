package model.prop.gem;

import model.Model;
import model.role.player.Player;

public class Gem implements Model{	
	
	/**
	 * 蓝宝石编码
	 */
	public static final int GEM_TYPE_BLUE = -62;
	
	/**
	 * 红宝石编码
	 */
	public static final int GEM_TYPE_RED = -61;
	
	/**
	 * 蓝宝石
	 */
	public static final Gem blue = new Gem(GEM_TYPE_BLUE);
	
	/**
	 * 红宝石
	 */
	public static final Gem red = new Gem(GEM_TYPE_RED);
	
	public static Gem getGem(int type) {
		switch (type) {
		case GEM_TYPE_BLUE:
			return blue;
		case GEM_TYPE_RED:
			return red;
		}
		return null;
	}
	
	private int type;
	
	private Gem(int type) {
		this.type = type;
	}

	@Override
	public boolean contact(Player player) {
		if(type == GEM_TYPE_BLUE) {
			player.addDefense(player.getNowFloor());
		}else {
			player.addAttack(player.getNowFloor());
		}
		return true;
	}

	@Override
	public int getType() {
		return type;
	}

}
