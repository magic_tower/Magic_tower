package model;


import model.role.player.Player;

/**
 * TODO		模型接口
 * @author	柳聪灵
 * @date	2019年1月13日
 * @time	上午9:05:30
 * @version	1.0
 */
public interface Model {
	
	/**
	 * 接触方法
	 * 玩家在每次移动时都会调用移动到的方格所持有的方法
	 * 实现的类可以通过调用或更改玩家的属性 或修改自身属性来达到互动的目的
	 * @return 返回是否可以移动
	 */
	boolean contact(Player player);
	
	/**
	 * 获得格子的类型
	 */
	int getType();
	
}
