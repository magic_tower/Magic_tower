package controller.game;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.data.PlayerFile;
import controller.data.SaveListener;
import model.role.player.Player;
import model.window.MapData;
import view.window.SideView;
import view.window.HintPopUpView;
import view.window.InofView;
import view.window.MainView;
import view.window.MapView;
import view.window.SelectRoleView;

/**
 * TODO		展示监听器
 * @author	李梦冰
 * @date	2019年1月14日
 * @time	下午3:40:17
 * @version	1.0
 */
public class ShowListener implements ActionListener {

	private MainView mainView;
	
	private Player player;
	
	private SelectRoleView view;

	public ShowListener(MainView mainView) {
		this.mainView = mainView;
	}
	
	public ShowListener(MainView mainView, int playerType, SelectRoleView view) {
		this.mainView = mainView;
		this.player = new Player(playerType);
		this.view = view;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(view != null) {
			view.dispose();
		}		
		if(player == null) {
			this.player = PlayerFile.readPlayer();
			if(player == null) {
				HintPopUpView.show(mainView, new Dimension(300, 210), new String[] {"没有游戏数据","请开始新游戏"});
				return;
			}
		}
		mainView.addWindowListener(new SaveListener(player));
		mainView.getContentPane().removeAll();
		Integer[][] integers = player.getMapDataList().get(player.getNowFloor() - 1);	
		MapView mapView = new MapView(integers);
		mainView.getContentPane().add(mapView);
		InofView inofView = new InofView(player);
		mainView.getContentPane().add(inofView);
		mainView.getContentPane().add(new SideView());
		mainView.addKeyListener(new GameListener(mainView ,MapData.getMapData(integers), mapView, player,inofView));
		mainView.requestFocus();//点击按钮后获取焦点
	}

}
