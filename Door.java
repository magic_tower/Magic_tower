package model.entity.door;

import model.Model;
import model.role.player.Player;

/**
 * TODO 门类
 * 
 * @author 柳聪灵
 * @date 2019年1月14日
 * @time 上午8:50:47
 * @site 
 * @version 1.0
 */
public class Door implements Model {

	/**
	 * 黄色门
	 */
	public static final int DOOR_TYPE_YELLOW = -11;

	/**
	 * 蓝色门
	 */
	public static final int DOOR_TYPE_BLUE = -12;

	/**
	 * 红色门
	 */
	public static final int DOOR_TYPE_RED = -13;

	/**
	 * 怪物门
	 */
	public static final int DOOR_TYPE_MONSTER = -14;

	/**
	 * 黄色门
	 */
	public static final Door doorYellow = new Door(DOOR_TYPE_YELLOW);

	/**
	 * 蓝色门
	 */
	public static final Door doorBlue = new Door(DOOR_TYPE_BLUE);

	/**
	 * 红色门
	 */
	public static final Door doorRed = new Door(DOOR_TYPE_RED);

	/**
	 * 怪物门
	 */
	public static final Door doorMonster = new Door(DOOR_TYPE_MONSTER);

	
	public static Door getDoor(int type) {
		switch (type) {
		case DOOR_TYPE_YELLOW:
			return doorYellow;
		case DOOR_TYPE_BLUE:
			return doorBlue;
		case DOOR_TYPE_RED:
			return doorRed;
		case DOOR_TYPE_MONSTER:
			return doorMonster;
		}
		return null;
	}
	
	/**
	 * 门类型
	 */
	private int type;

	private Door(int type) {
		this.type = type;
	}

	@Override
	public boolean contact(Player player) {
		if (type != DOOR_TYPE_MONSTER) {
			if (player.isVip() || player.useKey(type)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int getType() {
		return type;
	}
}
