package model.prop.weapon;


import model.Model;
import model.role.player.Player;

public class Shield implements Model ,Weapon{

	/**
	 * 铁盾编码
	 */
	public static final int SHIELD_TYPE_1 = -81;

	/**
	 * 银盾编码
	 */
	public static final int SHIELD_TYPE_2 = -82;

	/**
	 * 骑士盾编码
	 */
	public static final int SHIELD_TYPE_3 = -83;

	/**
	 * 圣盾编码
	 */
	public static final int SHIELD_TYPE_4 = -84;

	/**
	 * 神圣盾编码
	 */
	public static final int SHIELD_TYPE_5 = -85;
	
	/**
	 * 铁盾
	 */
	public static final Shield Shield_1 = new Shield(SHIELD_TYPE_1);
	
	/**
	 * 银盾
	 */
	public static final Shield Shield_2 = new Shield(SHIELD_TYPE_2);
	
	/**
	 * 骑士盾
	 */
	public static final Shield Shield_3 = new Shield(SHIELD_TYPE_3);
	
	/**
	 * 圣盾
	 */
	public static final Shield Shield_4 = new Shield(SHIELD_TYPE_4);
	
	/**
	 * 神圣盾
	 */
	public static final Shield Shield_5 = new Shield(SHIELD_TYPE_5);

	public static Shield getShield(int type) {
		switch (type) {
		case SHIELD_TYPE_1:
			return Shield_1;
		case SHIELD_TYPE_2:
			return Shield_3;
		case SHIELD_TYPE_3:
			return Shield_3;
		case SHIELD_TYPE_4:
			return Shield_4;
		case SHIELD_TYPE_5:
			return Shield_5;
		}
		return null;
	}
	
	private int defense;

	private String name;

	private int type;

	public Shield(int type) {
		this.type = type;
		switch (type) {
		case SHIELD_TYPE_1:
			defense = 10;
			name = "铁盾";
			break;
		case SHIELD_TYPE_2:
			defense = 20;
			name = "银盾";
			break;
		case SHIELD_TYPE_3:
			defense = 40;
			name = "骑士盾";
			break;
		case SHIELD_TYPE_4:
			defense = 50;
			name = "圣盾";
			break;
		case SHIELD_TYPE_5:
			defense = 100;
			name = "神圣盾";
			break;
		}
	}
	

	@Override
	public boolean contact(Player player) {
		player.addDefense(defense);
		return true;
	}

	@Override
	public int getType() {
		return type;
	}

	@Override
	public int getValue() {
		return defense;
	}

	@Override
	public String getName() {
		return name;
	}

}
