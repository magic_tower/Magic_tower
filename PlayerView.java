package view.role;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import model.role.player.Player;
import view.View;
import view.entity.FloorView;

public class PlayerView extends JPanel implements View{
	
	private static final long serialVersionUID = -5936676888152441271L;
	
	public static final int UP = 1;
	
	public static final int DOWN = 2;
	
	public static final int LEFT = 3;
	
	public static final int RIGHT = 4;
	
	private ImageIcon icon;
	
	private Timer timer;
	
	private int coord_x, coord_y;
	
	public void start(int direc) {
		switch (direc) {
		case UP:
			coord_y = 32 * 3;
			break;
		case DOWN:
			coord_y = 0;
			break;
		case LEFT:
			coord_y = 32;
			break;
		case RIGHT:
			coord_y = 32 * 2;
			break;
		}
		show();
	}
	
	public PlayerView(int type) {
		timer = new Timer(120, e -> repaint());
		switch (type) {
		case Player.PLAYER_TYPE_ONE:
			icon = new ImageIcon("src\\model\\role\\player\\Player1.png");
			break;
		case Player.PLAYER_TYPE_TOW:
			icon = new ImageIcon("src\\model\\role\\player\\Player2.png");
			break;
		case Player.PLAYER_TYPE_THREE:
			icon = new ImageIcon("src\\model\\role\\player\\Player3.png");
		}
		setSize(48, 48);
	}
	
	@Override
	public void show() {
		timer.start();
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(icon.getImage(), 0, 0, 48, 48, coord_x, coord_y, coord_x + 32, coord_y + 32, null);
		coord_x += 32;
		if(coord_x >= icon.getIconWidth()) {
			coord_x = 0;
			timer.stop();
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}
	
}