package model.entity.stairs;

import model.Model;
import model.role.player.Player;

/**
 * TODO		楼梯类
 * @author	柳聪灵
 * @date	2019年1月14日
 * @time	上午11:28:03
 * @version	1.0
 */
public class Stairs implements Model{
	
	/**
	 * 向上楼梯编码
	 */
	public static final int STAIRS_TYPE_UP = -21;

	/**
	 * 向下楼梯编码
	 */
	public static final int STAIRS_TYPE_DOWN = -22;
	
	/**
	 * 依附于下楼梯的 上楼出生点
	 */
	public static final int STAIRS_TYPE_UP_BIRTH = -25;

	/**
	 * 依附于上楼梯的 下楼出生点
	 */
	public static final int STAIRS_TYPE_DOWN_BIRTH = -26;
	
	/**
	 * 向上楼梯
	 */
	public static final Stairs stairsUp = new Stairs(STAIRS_TYPE_UP);
	
	/**
	 * 向下楼梯
	 */
	public static final Stairs stairsDown = new Stairs(STAIRS_TYPE_DOWN);
	
	/**
	 * 上楼出生点
	 */
	public static final Stairs UP_BIRTH = new Stairs(STAIRS_TYPE_UP_BIRTH);
	
	/**
	 * 下楼出生点
	 */
	public static final Stairs DOWN_BIRTH = new Stairs(STAIRS_TYPE_DOWN_BIRTH);
	
	
	public static Stairs getStairs(int type) {
		switch (type) {
		case STAIRS_TYPE_UP:
			return stairsUp;
		case STAIRS_TYPE_DOWN:
			return stairsDown;
		case STAIRS_TYPE_UP_BIRTH:
			return UP_BIRTH;
		case STAIRS_TYPE_DOWN_BIRTH:
			return DOWN_BIRTH;
		}
		return null;
	}
	
	private int type;
	
	private Stairs(int type) {
		this.type = type;
	}
 
	@Override
	public boolean contact(Player player) {
		if(type == STAIRS_TYPE_UP) {
			player.setNowFloor(player.getNowFloor() + 1);
			Integer[][] integers = player.getMapDataList().get(player.getNowFloor() -1);
			int i,j;
			for (i = 0; i < integers.length; i++) {
				for (j = 0; j < integers.length; j++) {
					if(integers[i][j] == STAIRS_TYPE_UP_BIRTH) {
						player.setCoord(j, i);
						return false;
					}
				}
			}
			return false;
		}else if(type == STAIRS_TYPE_DOWN) {
			player.setNowFloor(player.getNowFloor() - 1);
			Integer[][] integers = player.getMapDataList().get(player.getNowFloor() -1);
			int i,j;
			for (i = 0; i < integers.length; i++) {
				for (j = 0; j < integers.length; j++) {
					if(integers[i][j] == STAIRS_TYPE_DOWN_BIRTH) {
						player.setCoord(j, i);
						return false;
					}
				}
			}
			return false;
		}
		return true;
	}

	@Override
	public int getType() {
		return type;
	}

}
