package view.window;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import model.role.player.Player;

public class SelectRoleView extends JDialog {

	private static final long serialVersionUID = 9108955578342462964L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\window\\hint.png");

	private int selectType;
	
	public SelectRoleView(Frame owner) {
		super(owner, true);
		setSize(new Dimension(550, 500));
		setUndecorated(true);
		getRootPane().setOpaque(false);
		Rectangle bounds = owner.getBounds();
		setLocation((bounds.x + (bounds.width - getWidth()) / 2), (bounds.y + (bounds.height - getHeight()) / 2));
		JPanel jPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(IMG.getImage(), 0, 0, getWidth(), getHeight(), 0, 0, IMG.getIconWidth(),
						IMG.getIconHeight(), null);
			}
		};
		jPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 80, getHeight() / 4));
		setContentPane(jPanel);

		addTitle("请选择游戏角色");
		addSelectButton();
	}
	
	public int getSelectType() {
		return selectType;
	}

	private void addSelectButton() {
		SelectRole role1 = new SelectRole(Player.PLAYER_TYPE_ONE);
		role1.addMouseListener(new Move(role1));
		role1.addActionListener(e -> {
			selectType = Player.PLAYER_TYPE_ONE;
		});
		getContentPane().add(role1);
		SelectRole role2 = new SelectRole(Player.PLAYER_TYPE_TOW);
		role2.addMouseListener(new Move(role2));
		role1.addActionListener(e -> {
			selectType = Player.PLAYER_TYPE_TOW;
		});
		getContentPane().add(role2);
		SelectRole role3 = new SelectRole(Player.PLAYER_TYPE_THREE);
		role1.addActionListener(e -> {
			selectType = Player.PLAYER_TYPE_THREE;
		});
		role3.addMouseListener(new Move(role3));
		getContentPane().add(role3);
	}

	private void addTitle(String title) {
		JLabel label = new JLabel(title);
		label.setFont(new Font("幼圆", Font.BOLD, 40));
		label.setForeground(new Color(187, 192, 86));
		getContentPane().add(label);
	}
	
	public void addListeners(ActionListener[] listeners) {
		Component[] comp = getContentPane().getComponents();
		for (int i = 1; i < comp.length; i++) {
			((SelectRole)comp[i]).addActionListener(listeners[i - 1]);
		}
	}

	private class SelectRole extends JButton {

		private static final long serialVersionUID = -8595215883204684804L;

		private ImageIcon icon;

		private int coord;
		
		private Timer timer;
		
		public SelectRole(int type) {
			switch (type) {
			case Player.PLAYER_TYPE_ONE:
				icon = new ImageIcon("src\\model\\role\\player\\Player1.png");
				break;
			case Player.PLAYER_TYPE_TOW:
				icon = new ImageIcon("src\\model\\role\\player\\Player2.png");
				break;
			case Player.PLAYER_TYPE_THREE:
				icon = new ImageIcon("src\\model\\role\\player\\Player3.png");
			}
			timer = new Timer(200, e -> {
				repaint();
				setVisible(true);
			});
			setPreferredSize(new Dimension(100, 100));
			setHorizontalTextPosition(SwingConstants.CENTER);  
			setContentAreaFilled(false);//设置图片填满按钮所在的区域  
			setBorderPainted(false);//设置是否绘制边框  
			setBorder(null);//设置边框 
		}

		public void start() {
			timer.start();
		}
		
		public void stop() {
			timer.stop();
		}
		
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			g.drawImage(icon.getImage(), 0, 0, 100, 100,coord,0,coord + 32,32 ,null);
			coord += 32;
			if(coord == icon.getIconWidth()) {
				coord = 0;
			}
		}
	}

	private class Move implements MouseListener {
		SelectRole button;

		public Move(SelectRole button) {
			this.button = button;
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
			button.stop();
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			button.start();
		}

	}

}
