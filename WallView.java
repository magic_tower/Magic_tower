package view.entity;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import view.View;

public class WallView extends JPanel implements View{

	private static final long serialVersionUID = -6577079751365784568L;

	private static final ImageIcon IMG_TRUE = new ImageIcon("src\\model\\entity\\wall\\WallTrue.png");
	
	
	private static final ImageIcon IMG_FALSE = new ImageIcon("src\\model\\entity\\wall\\WallFalse.png");
	
	private boolean type;
	
	public WallView(boolean type) {
		setSize(48,48);
		this.type = type;
	}

	@Override
	public void show() {
		repaint();
	}

	@Override
	public void draw(Graphics g) {
		if(type) {
			g.drawImage(IMG_TRUE.getImage(), 0, 0, 48, 48, 0, 0, 32, 32, null);
		}else {
			g.drawImage(IMG_FALSE.getImage(), 0, 0, 48, 48, 0, 0, 32, 32, null);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		draw(g);
	}

}
