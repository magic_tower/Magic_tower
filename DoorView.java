package view.entity;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import model.entity.door.Door;
import view.View;


public class DoorView extends JPanel implements View{

	
	private static final long serialVersionUID = 8574537519185819208L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\entity\\door\\Door.png");
	
	private Timer timer;
	
	private int coord_x;
	
	private int coord_y;

	public DoorView(int type) {
		setSize(48, 48);
		this.timer = new Timer(120, e -> repaint());
		switch (type) {
		case Door.DOOR_TYPE_YELLOW:
			coord_x = 0;
			break;
		case Door.DOOR_TYPE_BLUE:
			coord_x = IMG.getIconWidth() / 4;
			break;
		case Door.DOOR_TYPE_RED:
			coord_x = IMG.getIconWidth() / 2;
			break;
		case Door.DOOR_TYPE_MONSTER:
			coord_x = IMG.getIconWidth() / 4 * 3;
		}
	}

	@Override
	public void show() {
		timer.start();
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, coord_x, coord_y, coord_x + 32,  coord_y + 32, null);
		if(timer.isRunning())
			coord_y += 32;
		if(coord_y - 32 >= IMG.getIconHeight()) {
			timer.stop();
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}

}
