package model.role.monster;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JFrame;

import model.Model;
import model.role.player.Player;
import view.role.MonsterView;

public class Monster implements Model{

	private static final Properties properties;
	
	static {
		properties = new Properties();
		try {
			properties.load(new FileInputStream("src/model/role/monster/monster.properties"));
		} catch (IOException e) {
		}
	}
	
	public static String getInof(int type) {
		return properties.getProperty(String.valueOf(type));
	}
	
	/**
	 * 攻击力
	 */
	private int attack;
	
	/**
	 * 金钱
	 */
	private int money;
	

	/**
	 * 经验
	 */
	private int exp;
	
	/**
	 * 血量
	 */
	private int red;
	
	/**
	 * 防御
	 */
	private int defense;
	
	/**
	 * 姓名
	 */
	private String name;
	
	/**
	 * 玩家损伤血量
	 */
	private int no_red;
	
	private int type;
	
	public Monster(int type) {
		this.type = type;
		String inof = getInof(type);
		String[] inofs = inof.split(",");
		name = inofs[2];
		attack = Integer.parseInt(inofs[3]);
		money = Integer.parseInt(inofs[4]);
		exp = Integer.parseInt(inofs[5]);
		red = Integer.parseInt(inofs[6]);
		defense = Integer.parseInt(inofs[7]);
	}

	@Override
	public boolean contact(Player player) {
		if(player.getAttack() < getDefense()) {
			return false;
		}else if(player.getDefense() > getAttack()){
			player.addExp(exp);
			player.addMoney(money);
			return true;
		}
		return pk(player);
	}

	private boolean pk(Player player) {
		int pred = player.getRed();
		while (true) {
			red -= (player.getAttack() - getDefense());
			if(red <= 0) {
				no_red = player.getRed() - pred;
				player.setRed(pred);
				player.addExp(exp);
				player.addMoney(money);
				return true;
			}
			pred -= (getAttack() - player.getDefense());
			if(pred <= 0) {
				return false;
			}
		}
	}

	@Override
	public int getType() {
		return type;
	}

	public int getAttack() {
		return attack;
	}

	public int getMoney() {
		return money;
	}

	public int getExp() {
		return exp;
	}

	public int getRed() {
		return red;
	}

	public int getDefense() {
		return defense;
	}

	public String getName() {
		return name;
	}
	
	public int getNo_red() {
		return no_red;
	}

	public static void main(String[] args) {
		properties.forEach((k,v) ->{
			JFrame frame = new JFrame();
			MonsterView view = new MonsterView(Integer.parseInt((String)k));
			frame.getContentPane().add(view);
			frame.setVisible(true);
			frame.setSize(200, 200);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			view.show();
		});

	}
}
