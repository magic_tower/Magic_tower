package view.window;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class HintPopUpView extends JDialog {

	private static final long serialVersionUID = 9108955578342462964L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\window\\hint.png");

	public static void show(Frame owner, Dimension dimension, String[] Inofs,int letfMove) {
		new HintPopUpView(owner, dimension, Inofs,letfMove).setVisible(true);
	}
	
	public static void show(Frame owner, Dimension dimension, String[] Inofs) {
		new HintPopUpView(owner, dimension, Inofs,0).setVisible(true);
	}
	
	private HintPopUpView(Frame owner, Dimension dimension, String[] Inofs,int letfMove) {
		super(owner, true);
		setSize(dimension);
		setUndecorated(true);
		getRootPane().setOpaque(false);
		Rectangle bounds = owner.getBounds();
		setLocation((bounds.x + (bounds.width - getWidth()) / 2) + letfMove, (bounds.y + (bounds.height - getHeight()) / 2));
		setPanel();
		addInfos(Inofs);
	}

	private void setPanel() {
		JPanel jPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(IMG.getImage(), 0, 0, getWidth(), getHeight(), 0, 0, IMG.getIconWidth(),
						IMG.getIconHeight(), null);
			}
		};
		jPanel.setLayout(new FlowLayout(FlowLayout.CENTER, getWidth(), 40));
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				dispose();
			}
		});
		setContentPane(jPanel);
	}

	private void addInfos(String[] Inofs) {
		Color color = new Color(187, 192, 86);
		Font font = new Font("幼圆", Font.BOLD, 18);

		for (String Inof : Inofs) {
			JLabel l = new JLabel(Inof);
			l.setFont(font);
			l.setForeground(color);
			getContentPane().add(l);
		}

		JLabel label = new JLabel("按任意键继续");
		label.setFont(font);
		label.setForeground(color);
		getContentPane().add(label);
		
	}

}
