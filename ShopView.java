package view.entity;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import model.entity.shop.Shop;
import view.View;

public class ShopView extends JPanel implements View{

	private static final long serialVersionUID = -120938798016373889L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\entity\\shop\\shop.png");
	
	private int coord;
	
	public ShopView(int type) {
		setSize(48, 48);
		switch (type) {
		case Shop.SHOP_TYPE_2:
			coord = 32;
			break;
		case Shop.SHOP_TYPE_3:
			coord = 64;
			break;
		}
	}
	
	
	@Override
	public void show() {
		repaint();
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, coord, 0,coord + 32, 32, null);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}
}
