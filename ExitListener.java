package controller.game;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.window.MainView;
import view.window.PopUpView;

/**
 * TODO 退出监听器
 * 
 * @author 李梦冰
 * @date 2019年1月15日
 * @time 下午2:21
 * @version 1.0
 */

public class ExitListener implements ActionListener {

	private MainView mainView;

	public ExitListener(MainView mainView) {
		this.mainView = mainView;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		PopUpView popUpView = new PopUpView(mainView, "是否退出游戏？", new Dimension(350, 300),
				new String[] { "返回游戏", "确认退出" });
		//添加对应监听器
		popUpView.addButtonsListener(new ActionListener[] { ac -> popUpView.dispose(), ac -> System.exit(0), });
		popUpView.setVisible(true);
	}
}