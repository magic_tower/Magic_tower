package view.prop;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.prop.key.Key;
import view.View;
import view.entity.FloorView;

public class KeyView extends JPanel implements View{

	private static final long serialVersionUID = -8050404727590625027L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\prop\\key\\key.png");
	
	private int coord;
	
	private boolean isBeck;
	
	public KeyView(int type) {
		this(type, true);
	}
	
	public KeyView(int type,boolean beck) {
		isBeck = beck;
		if(type ==  Key.KEY_TYPE_BLUE) {
			coord = 32;
		}else if(type == Key.KEY_TYPE_RED) {
			coord = 64;
		}
		setSize(48,48);
	}
	
	@Override
	public void show() {
		repaint();
	}


	@Override
	public void draw(Graphics g) {
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, coord, 32, coord + 32, 64, null);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(isBeck)
			FloorView.FLOORVIEW.draw(g);
		draw(g);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		KeyView view = new KeyView(Key.KEY_TYPE_BLUE);
		frame.getContentPane().add(view);
		frame.setVisible(true);
		frame.setSize(200, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.show();
	}


	
}
