package controller.game;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.role.player.Player;
import view.window.MainView;
import view.window.PopUpView;
import view.window.SelectRoleView;

/**
 * TODO		选择角色事件
 * @author	李梦冰
 * @date	2019年1月14日
 * @time	下午3:40:17
 * @version	1.0
 */
public class SelectRoleListener implements ActionListener{

	private MainView mainView;

	public SelectRoleListener(MainView mainView) {
		this.mainView = mainView;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		PopUpView popUpView = new PopUpView(mainView, "如果有存档将会覆盖！", new Dimension(450, 300), new String[] { "继续", "取消"});
		popUpView.addButtonsListener(new ActionListener[] {
				e1 -> {
					SelectRoleView view = new SelectRoleView(mainView);
					//三个角色监听器
					view.addListeners(new ShowListener[]  { 
							new ShowListener(mainView, Player.PLAYER_TYPE_ONE, view),
							new ShowListener(mainView, Player.PLAYER_TYPE_TWO, view),
							new ShowListener(mainView, Player.PLAYER_TYPE_THREE, view),
					});
					popUpView.dispose();
					view.setVisible(true);
					mainView.setVisible(true);
				},
				e1 ->{
					popUpView.dispose();
				}
		});
		popUpView.setVisible(true);
		
	}


	
}
