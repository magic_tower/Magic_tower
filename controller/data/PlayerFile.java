package controller.data;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.role.player.Player;

/**
 * TODO 文件I/O流
 * 
 * @author 李梦冰
 * @date 2019年1月15日
 * @time 上午8:06
 * @version 1.0
 */

public class PlayerFile {

	public static final void savePlayer(Player player) {//保存玩家数据
		//try-with-resource
		try ( FileOutputStream out = new FileOutputStream("player.dat");
				BufferedOutputStream bout = new BufferedOutputStream(out);
				ObjectOutputStream obout = new ObjectOutputStream(bout);) {
			obout.writeObject(player);
		} catch (IOException e) {
		}
	}

	public static final Player readPlayer() {//读取玩家数据
		Player player = null;
		try (FileInputStream in = new FileInputStream("player.dat");
				BufferedInputStream bin = new BufferedInputStream(in);
				ObjectInputStream obin = new ObjectInputStream(bin);) {
			player = (Player) obin.readObject();
		} catch (IOException e) {
		} catch (ClassNotFoundException e) {
		}
		return player;
	}

}
