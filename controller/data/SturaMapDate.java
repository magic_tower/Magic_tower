package controller.data;

import java.util.ArrayList;

import model.daoju.gem.Gem;
import model.daoju.key.Key;
import model.daoju.medicine.Medicine;
import model.daoju.weapon.Knife;
import model.daoju.weapon.Shield;
import model.scene.door.Door;
import model.scene.shop.Shop;
import model.scene.stairs.Stairs;

/**
 * TODO 游戏地图数据
 * 
 * @author 李梦冰
 * @date 2019年1月14日
 * @time 上午12:21
 * @version 1.0
 */

public class SturaMapDate {

	public static ArrayList<Integer[][]> srcDataToArray() {
		ArrayList<Integer[][]> mapDataList = new ArrayList<>();
		// 通过唯一的编码生成地图
		Integer[][][] a1 = new Integer[][][] {
				// 第一层
				{ { -21, Stairs.STAIRS_TYPE_DOWN_BIRTH, -100, -101, -100, 0, 0, 0, 0, 0, 0 },{ -31, -31, -31, -31, -31, -31, -31, -31, -31, -31, 0 },{ -41, 0, 0, Door.DOOR_TYPE_YELLOW, 0, -31, Gem.GEM_TYPE_RED, Key.KEY_TYPE_YELLOW, 0, -31, 0 },{ Key.KEY_TYPE_YELLOW, -103, Gem.GEM_TYPE_RED, -31, 0, -31, Gem.GEM_TYPE_BLUE, -41, 0, -31, 0 },{ -31, Door.DOOR_TYPE_YELLOW, -31, -31, 0, -31, -31, -31, -11, -31, 0 },{ -51, 0, 0, -31, 0, -11, -106, -111, -106, -31, 0 },{ Gem.GEM_TYPE_BLUE, -104, 0, -31, 0, -31, -31, -31, -31, -31, 0 },{ -31, -11, -31, -31, 0, 0, 0, 0, 0, 0, 0 },{ 0, 0, 0, -31, -31, Door.DOOR_TYPE_RED, -31, -31, -31, -11, -31 },{ -41, Shield.SHIELD_TYPE_1, -51, -31, Key.KEY_TYPE_RED, 0, 0, -31, 0, -106, 0 },{ -41, Knife.KNIFE_TYPE_1, -51, -31, 0, 0, 0, -31, -100, Medicine.MEDICINE_TYPE_BLUE, -100 }, },
				// 第二层
				{ { Stairs.STAIRS_TYPE_DOWN, 0, Door.DOOR_TYPE_RED, 0, 0, 0, 0, 0, 0, 0, 0 },{ Stairs.STAIRS_TYPE_UP_BIRTH, 0, -31, -31, 0, -111, 0, -111, 0, -31, -31 },{ 0, -31, -31, -31, -31, -31, 0, -31, -31, -31, -31 },{ 0, -31, -211, -51, -31, 0, 0, 0, -31, Gem.GEM_TYPE_RED, Gem.GEM_TYPE_RED },{ 0, -31, Key.KEY_TYPE_YELLOW, -51, Door.DOOR_TYPE_YELLOW, 0, 0, 0, Door.DOOR_TYPE_YELLOW,Gem.GEM_TYPE_RED, Gem.GEM_TYPE_RED },{ 0, -31, -31, -31, -31, -31, 0, -31, -31, -31, -31 },{ 0, -31, Key.KEY_TYPE_BLUE, -51, -31, 0, 0, 0, -31, Gem.GEM_TYPE_RED, Gem.GEM_TYPE_RED },{ 0, -31, Key.KEY_TYPE_BLUE, -51, Door.DOOR_TYPE_YELLOW, 0, 0, 0, Door.DOOR_TYPE_YELLOW,Gem.GEM_TYPE_BLUE, Gem.GEM_TYPE_BLUE },{ 0, -31, -31, -31, -31, -31, 0, -31, -31, -31, -31 },{ Stairs.STAIRS_TYPE_DOWN_BIRTH, -31, Key.KEY_TYPE_YELLOW, -51, -31, 0, 0, 0, -31,Knife.KNIFE_TYPE_5, Shield.SHIELD_TYPE_5 },{ Stairs.STAIRS_TYPE_UP, -31, Key.KEY_TYPE_YELLOW, -51, Door.DOOR_TYPE_YELLOW, 0, 0, 0,Door.DOOR_TYPE_YELLOW, 0, 0 }, },
				// 第三层
				{ { Key.KEY_TYPE_YELLOW, Gem.GEM_TYPE_BLUE, -31, Key.KEY_TYPE_YELLOW, Gem.GEM_TYPE_BLUE,Key.KEY_TYPE_YELLOW, -31, 0, -31, 0, Medicine.MEDICINE_TYPE_RED },{ 0, Medicine.MEDICINE_TYPE_RED, -31, Gem.GEM_TYPE_RED, Key.KEY_TYPE_YELLOW, Gem.GEM_TYPE_RED,-31, 0, Door.DOOR_TYPE_YELLOW, -106, Medicine.MEDICINE_TYPE_RED },{ -111, 0, -31, Key.KEY_TYPE_YELLOW, Gem.GEM_TYPE_BLUE, Key.KEY_TYPE_YELLOW, -31, 0, -31, -31,-31 },{ Door.DOOR_TYPE_YELLOW, -31, -31, -31, 0, -31, -31, 0, -31, 0, -111 },{ 0, 0, -106, 0, 0, 0, -100, 0, 0, 0, 0 },{ Door.DOOR_TYPE_YELLOW, -31, -31, 0, 0, 0, -31, 0, -31, -31, -31 },{ -103, 0, -31, -31, 0, -31, -31, 0, -31, 0, Medicine.MEDICINE_TYPE_RED },{ 0, Key.KEY_TYPE_YELLOW, -31, 0, 0, 0, -31, 0, Door.DOOR_TYPE_YELLOW, -111,Medicine.MEDICINE_TYPE_BLUE },{ Medicine.MEDICINE_TYPE_RED, Gem.GEM_TYPE_RED, -31, 0, 0, 0, -31, 0, -31, -31, -31 },{ -31, -31, -31, -31, 0, -31, -31, -101, -31, 0, 0 },{ Stairs.STAIRS_TYPE_DOWN, Stairs.STAIRS_TYPE_UP_BIRTH, 0, 0, 0, 0, -31, 0,Door.DOOR_TYPE_YELLOW, Stairs.STAIRS_TYPE_DOWN_BIRTH, Stairs.STAIRS_TYPE_UP }, },
				//第四层
				{ { 0, Key.KEY_TYPE_BLUE, 0, -31, Shop.SHOP_TYPE_1, -202, -203, -31, 0, Key.KEY_TYPE_RED, 0 },{ Medicine.MEDICINE_TYPE_RED, 0, Key.KEY_TYPE_YELLOW, -31, 0, 0, 0, -31, Key.KEY_TYPE_YELLOW, 0,Medicine.MEDICINE_TYPE_BLUE },{ 0, 0, 0, -31, 0, 0, 0, -31, 0, -104, 0 },{ -31, Door.DOOR_TYPE_YELLOW, -31, -31, -31, Door.DOOR_TYPE_BLUE, -31, -31, -31,Door.DOOR_TYPE_YELLOW, -31 },{ 0, -111, 0, Door.DOOR_TYPE_YELLOW, 0, -101, 0, 0, -103, 0, 0 },{ 0, 0, 0, -31, -31, -31, -31, -31, -31, -31, -31 }, { -101, 0, -100, 0, 0, 0, 0, 0, 0, 0, 0 },{ Door.DOOR_TYPE_YELLOW, -31, -31, -11, -31, -31, -31, -11, -31, -31, 0 },{ 0, -31, Gem.GEM_TYPE_BLUE, -106, Gem.GEM_TYPE_RED, -31, 0, -111, 0, -31, 0 },{ Stairs.STAIRS_TYPE_DOWN_BIRTH, -31, -101, Medicine.MEDICINE_TYPE_BLUE, -101, -31,Gem.GEM_TYPE_RED, 0, Gem.GEM_TYPE_RED, -31, Stairs.STAIRS_TYPE_UP_BIRTH },{ Stairs.STAIRS_TYPE_UP, -31, Key.KEY_TYPE_YELLOW, -101, Key.KEY_TYPE_YELLOW, -31,Gem.GEM_TYPE_RED, -100, Gem.GEM_TYPE_RED, -31, Stairs.STAIRS_TYPE_DOWN },},
				//第五层
				{ { Stairs.STAIRS_TYPE_UP, -31, 0, -100, Door.DOOR_TYPE_YELLOW, 0, -31, 0, 0, -11, 0 },{ 0, -31, 0, 0, -31, Key.KEY_TYPE_YELLOW, -31, -100, -100, -31, -101 },{ 0, -11, -106, 0, -31, 0, -31, -51, -51, -31, 0 },{ -31, -31, -11, -31, -31, -106, -31, -51, -51, -31, 0 },{ -51, 0, -111, Gem.GEM_TYPE_RED, -31, 0, -31, -31, -31, -31, 0 },{ -51, 0, 0, -106, -31, 0, -100, 0, 0, 0, 0 },{ -31, -104, -31, -31, -31, 0, -31, -31, -31, -31, -101 },{ 0, 0, 0, 0, -31, -100, -31, 0, 0, 0, 0 },{ Gem.GEM_TYPE_BLUE, Key.KEY_TYPE_YELLOW, Medicine.MEDICINE_TYPE_BLUE,Medicine.MEDICINE_TYPE_BLUE, -31, 0, -31, -11, -31, -31, -31 },{ -31, -31, -31, -31, -31, 0, -31, 0, -31, 0, 0 }, { Stairs.STAIRS_TYPE_DOWN,Stairs.STAIRS_TYPE_UP_BIRTH, 0, 0, 0, 0, -31, 0, -32, 0, Shield.SHIELD_TYPE_2 },},
				//第六层
				{ { Stairs.STAIRS_TYPE_DOWN, -31, -51, -51, -31, 0, -111, 0, -51, -100, 0 },{ Stairs.STAIRS_TYPE_UP_BIRTH, -31, -51, -51, -31, 0, -31, -31, -31, -31,Door.DOOR_TYPE_YELLOW },{ 0, -31, -31, -101, -31, 0, -31, Medicine.MEDICINE_TYPE_RED, 0, -103, 0 },{ 0, -11, -11, 0, -11, 0, -31, -102, -61, -62, -106 },{ 0, -31, -31, -31, -31, 0, -31, -31, -31, -31, -31 },{ 0, 0, -101, -111, 0, -51, 0, -103, -104, 0, 0 },{ -31, -31, -31, -31, -31, 0, -31, -31, -31, -31, 0 },{ -62, -61, -52, -51, -31, 0, -11, -11, 0, -11, 0 },{ -102, -212, -62, -61, -31, 0, -31, -31, -101, -31, -101 },{ -11, -31, -31, -31, -31, 0, -31, 0, -101, -31, Stairs.STAIRS_TYPE_DOWN_BIRTH },{ 0, -100, 0, 0, -103, 0, -31, -41, -41, -31, Stairs.STAIRS_TYPE_UP },},
				//第七层
				{ { Stairs.STAIRS_TYPE_UP, -31, Gem.GEM_TYPE_RED, -31, Knife.KNIFE_TYPE_2, -102, 0, -31, -51, -31,-100 },{ Stairs.STAIRS_TYPE_DOWN_BIRTH, -31, Medicine.MEDICINE_TYPE_RED, -31, -102, 0, 0, -31, -51,-31, -101 },{ 0, -31, -106, -31, 0, -31, -103, -31, -41, -31, -100 },{ 0, -31, 0, -31, 0, -31, 0, -31, 0, -31, 0 },{ -11, -31, -11, -31, -12, -31, -11, -31, -103, -31, -11 },{ 0, -104, 0, -111, 0, 0, 0, 0, 0, 0, 0 },{ -11, -31, -11, -31, -11, -31, -11, -31, -104, -31, -11 },{ 0, -31, 0, -31, 0, -31, 0, -31, 0, -31, 0 },{ 0, -31, 0, -31, -106, -31, -101, -31, Medicine.MEDICINE_TYPE_BLUE, -31, 0 },{ -100, -31, -100, -31, -51, -31, -101, -31, -51, -31, Stairs.STAIRS_TYPE_UP_BIRTH },{ 0, -101, 0, -31, -51, -31, -111, -31, -51, -31, Stairs.STAIRS_TYPE_DOWN },},
				//第八层
				{ { Stairs.STAIRS_TYPE_DOWN, 0, -11, -11, 0, Stairs.STAIRS_TYPE_UP, 0, -31, -201, -202, -203 },{ 0, 0, -31, -31, 0, 0, -100, -31, 0, Key.KEY_TYPE_RED, 0 },{ -11, -31, -31, -31, -31, -11, -31, -31, -41, 0, -42 },{ 0, -31, -51, -51, -51, 0, 0, -31, -31, -105, -31 },{ -41, -31, -31, -31, -31, -31, -111, -31, Knife.KNIFE_TYPE_3, 0, Shield.SHIELD_TYPE_3 },{ 0, -101, -100, -101, 0, -31, 0, -31, Gem.GEM_TYPE_RED, 0, Gem.GEM_TYPE_BLUE },{ -31, -31, -31, -31, -11, -31, 0, -31, -31, -11, -31 },{ 0, 0, 0, -106, 0, -103, 0, -111, 0, 0, 0 },{ -11, -31, -31, -31, -31, -31, -31, -31, -31, -31, -11 },{ -100, 0, -31, Gem.GEM_TYPE_RED, -51, -31, Key.KEY_TYPE_BLUE, -41, -31, 0, -103 },{ 0, -106, Door.DOOR_TYPE_BLUE, -51, Gem.GEM_TYPE_BLUE, -31, -51, 0, -11, -104, 0 },},
				//第九层
				{ { 0, 0, -103, -11, 0, Stairs.STAIRS_TYPE_DOWN, 0, -11, -103, -103, -42 },{ 0, -51, 0, -31, 0, 0, 0, -31, 0, -100, 0 },{ -104, -31, -31, -31, -31, -12, -31, -31, -31, -31, 0 },{ 0, -51, 0, -31, -51, 0, -51, -11, -11, 0, Shield.SHIELD_TYPE_4 },{ -62, 0, -106, -11, 0, -61, 0, -31, -31, -31, -31 },{ -31, -31, -31, -31, -31, -31, 0, -32, -104, 0, Gem.GEM_TYPE_BLUE },{ -51, 0, -11, -104, -51, -31, 0, -31, Knife.KNIFE_TYPE_4, -31, 0 },{ -104, 0, -31, 0, 0, -31, 0, -31, -31, -31, -11 },{ -11, -31, -31, -31, -11, -31, 0, -31, -52, 0, -111 },{ Stairs.STAIRS_TYPE_DOWN_BIRTH, -41, -31, 0, -103, -31, -106, -31, 0, -104, 0 },{ Stairs.STAIRS_TYPE_UP, 0, Door.DOOR_TYPE_BLUE, 0, 0, -11, 0, -11, -111, 0, -41 },},
				//第十层
				{ { 0, 0, 0, 0, -131, -133, -132, 0, 0, 0, 0 }, { -31, -31, -31, -31, 0, -130, 0, -31, -31, -31, -31 },{ -122, -123, -122, -31, -31, -32, -31, -31, -119, -120, -119 },{ 0, -121, 0, -12, -112, -110, -112, -12, 0, -118, 0 },{ -31, -32, -31, -31, 0, -109, 0, -31, -31, -32, -31 },{ -116, -117, -116, -31, 0, 0, 0, -31, -113, -113, -113 },{ 0, -115, 0, -31, -31, 0, -31, -31, 0, -114, 0 },{ -11, -31, -12, -31, -31, Door.DOOR_TYPE_RED, -31, -31, -12, -31, -11 },{ 0, -31, 0, -31, -108, 0, -108, -31, 0, -31, 0 },{ Stairs.STAIRS_TYPE_UP_BIRTH, -105, 0, -107, 0, 0, 0, -107, 0, -105, 0 },{ Stairs.STAIRS_TYPE_DOWN, -31, 0, 0, 0, 0, 0, 0, 0, -31, 0 }, } };
				//待续......

		for (Integer[][] integers : a1) {
			mapDataList.add(integers);
		}
		return mapDataList;
	}
}
