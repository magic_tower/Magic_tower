package controller;

import java.awt.EventQueue;// 是一个与平台无关的类，它将来自于底层同位体类和受信任的应用程序类的事件列入队列。
import java.awt.event.ActionListener;
import controller.game.ExitListener;
import controller.game.SelectRoleListener;
import controller.game.ShowListener;
import view.window.SelectPatternView;
import view.window.MainView;

/**
 * TODO 游戏入口
 * 
 * @author 李梦冰
 * @date 2019年1月15日
 * @time 上午10:21
 * @version 1.0
 */

public class MainSystem {

	public static void startGame() {
		//创建线程
		EventQueue.invokeLater(new Runnable() {
			@Override
			//线程的执行
			public void run() {
				try {
					MainView mainView = new MainView();
					SelectPatternView selectView = new SelectPatternView();
					//添加监听器
					selectView.addButtonsListener(new ActionListener[] { 
							new SelectRoleListener(mainView),
							new ShowListener(mainView), new ExitListener(mainView) 
							});
					mainView.add(selectView);
					//显示
					mainView.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void main(String[] args) {
		MainSystem.startGame();
	}

}