package model.prop.weapon;


import model.Model;
import model.role.player.Player;

/**
 * TODO 刀类
 * 
 * @author 柳聪灵 
 * @date 2019年1月14日
 * @time 下午14:01:23
 * @version 1.0
 */
public class Knife implements Model,Weapon {

	/**
	 * 铁剑编码
	 */
	public static final int KNIFE_TYPE_1 = -71;

	/**
	 * 银剑编码
	 */
	public static final int KNIFE_TYPE_2 = -72;

	/**
	 * 骑士剑编码
	 */
	public static final int KNIFE_TYPE_3 = -73;

	/**
	 * 圣剑编码
	 */
	public static final int KNIFE_TYPE_4 = -74;

	/**
	 * 神圣剑编码
	 */
	public static final int KNIFE_TYPE_5 = -75;
	
	/**
	 * 铁剑
	 */
	public static final Knife KNIFE_1 = new Knife(KNIFE_TYPE_1);
	
	/**
	 * 银剑
	 */
	public static final Knife KNIFE_2 = new Knife(KNIFE_TYPE_2);
	
	/**
	 * 骑士剑
	 */
	public static final Knife KNIFE_3 = new Knife(KNIFE_TYPE_3);
	
	/**
	 * 圣剑
	 */
	public static final Knife KNIFE_4 = new Knife(KNIFE_TYPE_4);
	
	/**
	 * 神圣剑
	 */
	public static final Knife KNIFE_5 = new Knife(KNIFE_TYPE_5);

	public static Knife getKnife(int type) {
		switch (type) {
		case KNIFE_TYPE_1:
			return KNIFE_1;
		case KNIFE_TYPE_2:
			return KNIFE_2;
		case KNIFE_TYPE_3:
			return KNIFE_3;
		case KNIFE_TYPE_4:
			return KNIFE_4;
		case KNIFE_TYPE_5:
			return KNIFE_5;
		}
		return null;
	}
	
	private int attack;

	private String name;

	private int type;

	private Knife(int type) {
		this.type = type;
		switch (type) {
		case KNIFE_TYPE_1:
			attack = 10;
			name = "铁剑";
			break;
		case KNIFE_TYPE_2:
			attack = 20;
			name = "银剑";
			break;
		case KNIFE_TYPE_3:
			attack = 40;
			name = "骑士剑";
			break;
		case KNIFE_TYPE_4:
			attack = 50;
			name = "圣剑";
			break;
		case KNIFE_TYPE_5:
			attack = 100;
			name = "神圣剑";
			break;
		}
	}
	
	@Override
	public boolean contact(Player player) {
		player.addAttack(attack);
		return true;
	}

	@Override
	public int getType() {
		return type;
	}

	@Override
	public int getValue() {
		return attack;
		
	}

	@Override
	public String getName() {
		return name;
	}
}
