package model.window;

import model.Model;
import model.entity.door.Door;
import model.entity.floor.Floor;
import model.entity.shop.Shop;
import model.entity.stairs.Stairs;
import model.entity.wall.Wall;
import model.prop.gem.Gem;
import model.prop.key.Key;
import model.prop.medicine.Medicine;
import model.prop.weapon.Knife;
import model.prop.weapon.Shield;
import model.role.monster.Monster;
import model.role.npc.Npc;

public class MapData {
	
	public static Model[][] getMapData(Integer[][] mapData) {
		Model[][] models = new Model[mapData.length][mapData[0].length]; 
		for (int i = 0; i < mapData.length; i++) {
			for (int j = 0; j < mapData[i].length; j++) {
				if(mapData[i][j] <= -11 && mapData[i][j] >= -20) {
					models[i][j] = Door.getDoor(mapData[i][j]);
				}else if(mapData[i][j] <= -21 && mapData[i][j] >= -30) {
					models[i][j] = Stairs.getStairs(mapData[i][j]);
				}else if(mapData[i][j] <= -31 && mapData[i][j] >= -40) {
					models[i][j] = Wall.getWall(mapData[i][j]);
				}else if(mapData[i][j] <= -41  && mapData[i][j] >= -50) {
					models[i][j] = Medicine.getMedicine(mapData[i][j]);
				}else if(mapData[i][j] <= -51  && mapData[i][j] >= -60) {
					models[i][j] = Key.getKey(mapData[i][j]);
				}else if(mapData[i][j] <= -61  && mapData[i][j] >= -70) {
					models[i][j] = Gem.getGem(mapData[i][j]);
				}else if(mapData[i][j] <= -71  && mapData[i][j] >= -80) {
					models[i][j] = Knife.getKnife(mapData[i][j]);
				}else if(mapData[i][j] <= -81  && mapData[i][j] >= -90) {
					models[i][j] = Shield.getShield(mapData[i][j]);
				}else if(mapData[i][j] <= -100  && mapData[i][j] >= -200) {
					models[i][j] = new Monster(mapData[i][j]);
				}else if(mapData[i][j] <= -201  && mapData[i][j] >= -210){
					models[i][j] = Shop.getShop(mapData[i][j]);
				}else if(mapData[i][j] <= -211  && mapData[i][j] >= -300){
					models[i][j] = Npc.getNpc(mapData[i][j]);
				}else {
					models[i][j] = Floor.floor;
				} 
			}
		}
		return models;
	}
	
}
