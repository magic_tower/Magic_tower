package model.entity.wall;

import model.Model;
import model.role.player.Player;

/**
 * TODO		墙类
 * @author	柳聪灵
 * @date	2019年1月14日
 * @time	下午12:26:08
 * @version	1.0
 */
public class Wall implements Model{
	
	/**
	 * 真实墙 编码
	 */
	public static final int WALL_TYPE_TRUE = -31;
	
	
	/**
	 * 假墙
	 */
	public static final int WALL_TYPE_FALSE = -32;
	
	/**
	 * 真墙
	 */
	public static final Wall wallTrue = new Wall(WALL_TYPE_TRUE);
	
	
	/**
	 * 假墙
	 */
	public static final Wall wallFalse = new Wall(WALL_TYPE_FALSE);
	
	public static Wall getWall(int type) {
		switch (type) {
		case WALL_TYPE_TRUE:
			return wallTrue;
		case WALL_TYPE_FALSE:
			return wallFalse;
		}
		return null;
	}
	
	private int type;

	private Wall(int type) {
		this.type = type;
	}


	@Override
	public boolean contact(Player player) {
		if(type == WALL_TYPE_TRUE) {
			return false;
		}
		return true;
	}

	@Override
	public int getType() {
		return type;
	}
	
}
