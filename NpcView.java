package view.role;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import view.View;
import view.entity.FloorView;

/**
 * 	Npc
 */

public class NpcView extends JPanel implements View{

	private static final long serialVersionUID = -120938798016373889L;

	private static final ImageIcon IMG = new ImageIcon("src/model/role/npc/npc.png");
	
	public NpcView(int type) {
		setSize(48, 48);
	}
	
	@Override
	public void show() {
		repaint();
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, 0, 0, 32, 32, null);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}
}