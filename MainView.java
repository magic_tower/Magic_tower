package view.window;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class MainView extends JFrame{
	
	private static final long serialVersionUID = 7512017135073147684L;

	public static final int WIDTH = 48 * 18 + 5;

	public static final int HEIGHT = 48 * 13 + 28;
	
	public MainView() {
		getContentPane().setLayout(null);
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((dim.width - WIDTH) / 2, (dim.height - HEIGHT) / 2);
		setVisible(true);
		setResizable(false);
	}
	
}
