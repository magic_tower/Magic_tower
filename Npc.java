package model.role.npc;

import model.Model;
import model.role.player.Player;

public class Npc implements Model{
	
	/**
	 * 对话npc
	 */
	public static final int NPC_TYPE_DIALOGUE = -211;
	

	/**
	 * 商人
	 */
	public static final int NPC_TYPE_MERCHANT = -212;
	
	public static final Npc dialogue = new Npc(NPC_TYPE_DIALOGUE);

	public static final Npc merchant = new Npc(NPC_TYPE_MERCHANT);

	private int type;
	
	public static Npc getNpc(int type) {
		switch (type) {
		case NPC_TYPE_DIALOGUE:
			return dialogue;
		case NPC_TYPE_MERCHANT:
			return merchant;
		}
		return null;
	}
	
	@Override
	public boolean contact(Player player) {
		return false;
	}
	
	private Npc(int type) {
		this.type = type;
	}
	
	@Override
	public int getType() {
		return type;
	}

}
