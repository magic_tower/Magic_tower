package view.prop;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.prop.weapon.Knife;
import view.View;
import view.entity.FloorView;

public class KnifeView extends JPanel implements View{

	
	private static final long serialVersionUID = -1387838913852989520L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\prop\\weapon\\weapon.png");
	
	private int coord_x, coord_y;
	
	public KnifeView(int type) {
		switch (type) {
		case Knife.KNIFE_TYPE_2:
			coord_x = 32;
			break;
		case Knife.KNIFE_TYPE_3:
			coord_x = 32 * 2;
			break;
		case Knife.KNIFE_TYPE_4:
			coord_x = 32 * 3;
			break;
		case Knife.KNIFE_TYPE_5:
			coord_y = 32;
			break;
		}
		setSize(48,48);
	}
	
	@Override
	public void show() {
		repaint();
	}


	@Override
	public void draw(Graphics g) {
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, coord_x, coord_y, coord_x + 32,coord_y + 32, null);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		KnifeView view = new KnifeView(Knife.KNIFE_TYPE_3);
		frame.getContentPane().add(view);
		frame.setVisible(true);
		frame.setSize(200, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		view.show();
	}

}
