package model.prop.weapon;

public interface Weapon {
	
	
	/**
	 * 获得武器名
	 */
	String getName();

	/**
	 * 获得武器属性值
	 */
	int getValue();
	
	
}
