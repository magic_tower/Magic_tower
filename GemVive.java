package view.prop;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import model.prop.gem.Gem;
import view.View;
import view.entity.FloorView;

public class GemVive extends JPanel implements View{

	
	private static final long serialVersionUID = -1387838913852989520L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\prop\\gem\\Gem.png");
	
	private int coord;
	
	public GemVive(int type) {
		if(type == Gem.GEM_TYPE_BLUE) {
			coord = 32;
		}
		setSize(48,48);
	}
	
	@Override
	public void show() {
		repaint();
	}


	@Override
	public void draw(Graphics g) {
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, coord, 0, coord + 32, 32, null);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}

}
