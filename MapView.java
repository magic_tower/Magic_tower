package view.window;

import java.awt.GridLayout;

import javax.swing.JPanel;

import view.entity.DoorView;
import view.entity.FloorView;
import view.entity.ShopView;
import view.entity.StairsView;
import view.entity.WallView;
import view.prop.GemVive;
import view.prop.KeyView;
import view.prop.KnifeView;
import view.prop.MedicineView;
import view.prop.ShieldView;
import view.role.MonsterView;
import view.role.NpcView;

public class MapView extends JPanel{

	private static final long serialVersionUID = 5194956571714831597L;
	
	public MapView(Integer[][] mapData) {
		setLayout(new GridLayout(11, 11));
		for (int i = 0; i < mapData.length; i++) {
			for (int j = 0; j < mapData[i].length; j++) {
				if(mapData[i][j] <= -11 && mapData[i][j] >= -20) {
					add(new DoorView(mapData[i][j]));
				}else if(mapData[i][j] <= -21 && mapData[i][j] >= -30) {
					add(new StairsView(mapData[i][j]));
				}else if(mapData[i][j] <= -31 && mapData[i][j] >= -40) {
					add(new WallView(mapData[i][j] == -31));
				}else if(mapData[i][j] <= -41  && mapData[i][j] >= -50) {
					add(new MedicineView(mapData[i][j]));
				}else if(mapData[i][j] <= -51  && mapData[i][j] >= -60) {
					add(new KeyView(mapData[i][j]));
				}else if(mapData[i][j] <= -61  && mapData[i][j] >= -70) {
					add(new GemVive(mapData[i][j]));
				}else if(mapData[i][j] <= -71  && mapData[i][j] >= -80) {
					add(new KnifeView(mapData[i][j]));
				}else if(mapData[i][j] <= -81  && mapData[i][j] >= -90) {
					add(new ShieldView(mapData[i][j]));
				}else if(mapData[i][j] <= -100  && mapData[i][j] >= -200) {
					MonsterView monster = new MonsterView(mapData[i][j]);
					monster.show();
					add(monster);
				}else if(mapData[i][j] <= -201  && mapData[i][j] >= -210) {
					add(new ShopView(mapData[i][j]));
				}else if(mapData[i][j] <= -211  && mapData[i][j] >= -300) {
					add(new NpcView(mapData[i][j]));
				}else {
					add(new FloorView());
				} 
			}
		}
		setBounds(48 * 6, 48, 48 * 11, MainView.HEIGHT - 28 - 48 * 2);
	}
}
