package model.entity.floor;

import model.Model;
import model.role.player.Player;

/**
 * TODO		地板
 * @author	柳聪灵
 * @date	2019年1月14日
 * @time	上午10:29:37
 * @version	1.0
 */
public class Floor implements Model{
	
	public static final int TYPE_FLOOR = 0;
	
	public static final Floor floor = new Floor();

	@Override
	public boolean contact(Player player) {
		return true;
	}

	private Floor() {
		
	}
	
	@Override
	public int getType() {
		return TYPE_FLOOR;
	}

}
