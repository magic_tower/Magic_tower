package model.prop.key;

import model.Model;
import model.role.player.Player;

public class Key implements Model{
	
	/**
	 * 黄钥匙编码
	 */
	public static final int KEY_TYPE_YELLOW = -51;
	
	/**
	 * 蓝钥匙编码
	 */
	public static final int KEY_TYPE_BLUE = -52;
	
	/**
	 * 红钥匙编码
	 */
	public static final int KEY_TYPE_RED = -53;

	/**
	 * 红钥匙
	 */
	public static final Key keyRed = new Key(KEY_TYPE_RED);
	
	/**
	 * 蓝钥匙
	 */
	public static final Key keyBlue = new Key(KEY_TYPE_BLUE);
	
	/**
	 * 黄钥匙
	 */
	public static final Key keyYellow = new Key(KEY_TYPE_YELLOW);
	
	public static Key getKey(int type) {
		switch (type) {
		case KEY_TYPE_YELLOW:
			return keyYellow;
		case KEY_TYPE_BLUE:
			return keyBlue;
		case KEY_TYPE_RED:
			return keyRed;
		}
		return null;
	}
	
	private int type;
	
	private Key(int type) {
		this.type = type;
	}

	@Override
	public boolean contact(Player player) {
		player.addKey(type);
		return true;
	}

	@Override
	public int getType() {
		return type;
	}
	
}
