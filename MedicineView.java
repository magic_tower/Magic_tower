package view.prop;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import model.prop.medicine.Medicine;
import view.View;
import view.entity.FloorView;

public class MedicineView extends JPanel implements View{

	private static final long serialVersionUID = -6577079751365784568L;

	private static final ImageIcon IMG_RED = new ImageIcon("src\\model\\prop\\medicine\\red.png");
	
	private static final ImageIcon IMG_BLUE = new ImageIcon("src\\model\\prop\\medicine\\blue.png");
	
	private int type;
	
	public MedicineView(int type) {
		this.type = type;
		setSize(48,48);
	}

	@Override
	public void show() {
		repaint();
	}

	@Override
	public void draw(Graphics g) {
		if(type == Medicine.MEDICINE_TYPE_RED) {
			g.drawImage(IMG_RED.getImage(), 0, 0, 48, 48, 0, 0, 32, 32, null);
		}else {
			g.drawImage(IMG_BLUE.getImage(), 0, 0, 48, 48, 0, 0, 32, 32, null);
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}

}
