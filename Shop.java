package model.entity.shop;

import model.Model;
import model.role.player.Player;

public class Shop implements Model{

	/**
	 * 商店1编码
	 */
	public static final int SHOP_TYPE_1 = -201;

	/**
	 * 商店2编码
	 */
	public static final int SHOP_TYPE_2 = -202;
	/**
	 * 商店3编码
	 */
	public static final int SHOP_TYPE_3 = -203;
	
	/**
	 * 商店1
	 */
	public static final Shop shop1 = new Shop(SHOP_TYPE_1);

	/**
	 * 商店2
	 */
	public static final Shop shop2 = new Shop(SHOP_TYPE_2);

	/**
	 * 商店3
	 */
	public static final Shop shop3 = new Shop(SHOP_TYPE_3);


	
	public static Shop getShop(int type) {
		switch (type) {
		case SHOP_TYPE_1:
			return shop1;
		case SHOP_TYPE_2:
			return shop2;
		case SHOP_TYPE_3:
			return shop3;
		}
		return null;
	}
	
	private int type;

	private Shop(int type) {
		this.type = type;
	}

	@Override
	public boolean contact(Player player) {
		return false;
	}

	@Override
	public int getType() {
		return type;
	}
}
