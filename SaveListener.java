package controller.data;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import model.role.player.Player;

/**
 * TODO 存档监听器
 * 
 * @author 李梦冰
 * @date 2019年1月14日
 * @time 下午 6:52
 * @version 1.0
 */

public class SaveListener implements WindowListener {

	private Player player;

	public SaveListener(Player player) {
		this.player = player;
	}

	@Override
	public void windowClosing(WindowEvent e) {
		PlayerFile.savePlayer(player);
	}

	@Override
	public void windowOpened(WindowEvent e) {

	}

	@Override
	public void windowClosed(WindowEvent e) {

	}

	@Override
	public void windowIconified(WindowEvent e) {

	}

	@Override
	public void windowDeiconified(WindowEvent e) {

	}

	@Override
	public void windowActivated(WindowEvent e) {

	}

	@Override
	public void windowDeactivated(WindowEvent e) {

	}

}