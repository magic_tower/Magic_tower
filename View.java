package view;

import java.awt.Graphics;

public interface View {
	
	/**
	 * 显示
	 */
	void show();
	
	/**
	 * 绘制方法
	 */
	void draw(Graphics g);
}
