package view.role;

import java.awt.Graphics;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import model.role.monster.Monster;
import view.View;
import view.entity.FloorView;

public class MonsterView extends JPanel implements View{
	
	private static final long serialVersionUID = 1L;

	private static final HashMap<String, ImageIcon> ICON_MAP;
	
	static {
		ICON_MAP = new HashMap<>();
	}
	
	private ImageIcon icon;
	
	private int coord_x,coord_y;
	
	private Timer timer;
	
	public MonsterView(int type) {
		timer = new Timer(300, e -> repaint());
		String inof = Monster.getInof(type);
		String[] inofs = inof.split(",");
		if(!ICON_MAP.containsKey(inofs[0])) {
			ICON_MAP.put(inofs[0], new ImageIcon(inofs[0]));
		}
		icon = ICON_MAP.get(inofs[0]);
		coord_y = Integer.parseInt(inofs[1]) * 32;
	}

	@Override
	public void show() {
		timer.start();
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(icon.getImage(), 0, 0, 48, 48, coord_x, coord_y, coord_x + 32, coord_y + 32,null);
		coord_x += 32;
		if(coord_x == icon.getIconWidth()) {
			coord_x = 0;
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}
	
}
