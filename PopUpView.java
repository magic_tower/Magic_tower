package view.window;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PopUpView extends JDialog {

	private static final long serialVersionUID = 9108955578342462964L;
	
	private static final ImageIcon IMG = new ImageIcon("src\\model\\window\\hint.png");
	
	public PopUpView(Frame owner, String title, Dimension dimension, String[] buttonInof) {
		this(owner, title, dimension,0 ,buttonInof);
	}

	public PopUpView(Frame owner, String title, Dimension dimension, int moveLocation, String[] buttonInof) {
		super(owner, true);
		setSize(dimension);
		setUndecorated(true);
		getRootPane().setOpaque(false);
		Rectangle bounds = owner.getBounds();
		setLocation((bounds.x + (bounds.width - getWidth()) / 2) + moveLocation, (bounds.y + (bounds.height - getHeight()) / 2));
		JPanel jPanel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(IMG.getImage(), 0, 0, getWidth(), getHeight(), 0, 0, IMG.getIconWidth(),
						IMG.getIconHeight(), null);
			 }
		};
		jPanel.setLayout(new FlowLayout(FlowLayout.CENTER, getWidth(), getHeight() / 5));
		setContentPane(jPanel);
		addTitle(title);
		addButton(owner, title, buttonInof);
	}

	private void addButton(Frame owner, String title, String[] buttonInof) {

		Font font = new Font("华文新魏", Font.BOLD, 36);
		JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, getHeight(), 8));
		panel.setPreferredSize(new Dimension(getWidth(),getHeight()));
		panel.setOpaque(false);
		for (String string : buttonInof) {
			JButton button = new JButton(string);
			button.addMouseListener(new MoveColor(button));
			button.setContentAreaFilled(false);// 设置图片填满按钮所在的区域
			button.setBorderPainted(false);// 设置是否绘制边框
			button.setBorder(null);// 设置边框
			button.setForeground(Color.LIGHT_GRAY);
			button.setFont(font);
			panel.add(button);
		}

		getContentPane().add(panel);
	}
	
	public void addButtonsListener(ActionListener[] listeners) {
		Component[] coms = ((JPanel)(getContentPane().getComponents()[1])).getComponents();
		for (int i = 0; i < coms.length; i++) {
			((JButton)coms[i]).addActionListener(listeners[i]);
		}
	}

	private void addTitle(String title) {
		JLabel label = new JLabel(title);
		label.setFont(new Font("幼圆", Font.BOLD, 36));
		label.setForeground(new Color(187, 192, 86));
		getContentPane().add(label);
	}

	private class MoveColor implements MouseListener {
		JButton button;

		public MoveColor(JButton button) {
			this.button = button;
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
			button.setForeground(Color.LIGHT_GRAY);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			button.setForeground(Color.black);
		}

	}

}
