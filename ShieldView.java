package view.prop;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import model.prop.weapon.Shield;
import view.View;
import view.entity.FloorView;

public class ShieldView extends JPanel implements View{

	
	private static final long serialVersionUID = -1387838913852989520L;

	private static final ImageIcon IMG = new ImageIcon("src\\model\\prop\\weapon\\weapon.png");
	
	private int coord_x, coord_y;
	
	public ShieldView(int type) {
		coord_y = 32 * 2;
		switch (type) {
		case Shield.SHIELD_TYPE_2:
			coord_x = 32;
			break;
		case Shield.SHIELD_TYPE_3:
			coord_x = 32 * 2;
			break;
		case Shield.SHIELD_TYPE_4:
			coord_x = 32 * 3;
			break;
		case Shield.SHIELD_TYPE_5:
			coord_y = 32 * 3;
			break;
		}
		setSize(48,48);
	}
	
	@Override
	public void show() {
		repaint();
	}


	@Override
	public void draw(Graphics g) {
		g.drawImage(IMG.getImage(), 0, 0, 48, 48, coord_x, coord_y, coord_x + 32,coord_y + 32, null);
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		FloorView.FLOORVIEW.draw(g);
		draw(g);
	}

}
