package model.prop.medicine;

import model.Model;
import model.role.player.Player;

public class Medicine implements Model{

	/**
	 * 红药编码
	 */
	public static final int MEDICINE_TYPE_RED = -41;
	
	/**
	 * 蓝药编码
	 */
	public static final int MEDICINE_TYPE_BLUE = -42;
	
	/**
	 * 红药
	 */
	public static final Medicine red = new Medicine(MEDICINE_TYPE_RED);
	
	/**
	 * 蓝药
	 */
	public static final Medicine blue = new Medicine(MEDICINE_TYPE_BLUE);
	
	public static Medicine getMedicine(int type) {
		switch (type) {
		case MEDICINE_TYPE_RED:
			return red;
		case MEDICINE_TYPE_BLUE:
			return blue;
		}
		return null;
	}
	
	
	private int type;
	
	private Medicine(int type) {
		this.type = type;
	}
	
	@Override
	public boolean contact(Player player) {
		int addSize = player.getNowFloor() * 50;
		if(type == MEDICINE_TYPE_BLUE) {
			addSize *= 4;
		}
		player.setRed(player.getRed() + addSize);
		return true;
	}

	@Override
	public int getType() {
		return type;
	}

}
